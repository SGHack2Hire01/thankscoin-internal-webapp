'use strict';

angular.module('myApp.dashboard')

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/dashboard', {
            templateUrl: 'common/dashboard.view.html',
            controller: 'dashboardCtrl'
        });


    }])

    .controller('dashboardCtrl',['$rootScope', '$location', '$scope','$http','config',function($rootScope,$location,$scope, $http,config) {
        var dashboardCtrl = this;
        dashboardCtrl.query = query;

        (function initController() {
            query();
        })();

        $rootScope.transfers = [];
        function query() {
            $http.get(config.apiUrl+'/accounts/'+'7b79b8cde46035bc7db2ae4250748f3a'+'/history').then(handleSuccessQueryTran, handleErrQueryTran);

        }

        function handleSuccessQueryTran(response) {
            $rootScope.transfers = response.data;
            console.log($rootScope.transfers);
            $scope.dataLoading = false;
        }

        function handleErrQueryTran(response) {
            $rootScope.transfers = [];
        }






    }]);
