'use strict';

angular.module('myApp.dashboard')

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/transfer', {
            templateUrl: 'transfer/transfer.view.html',
            controller: 'transferCtrl'
        });


    }])

    .controller('transferCtrl',['$rootScope', '$location', '$http','$scope','config',function($rootScope,$location,$http,$scope,config) {

        $scope.createTransfer =function () {
            $scope.dataLoading = true;
            var transfer_id= $('#transfer-id').val();
            var transfer_coin= $('#transfer-coin').val();
            var transfer_description = $('#transfer-description').val();
            var transfer_signature = $('#transfer-signature').val();
            console.log(transfer_coin,transfer_id);

            $http.post(config.apiUrl+'accounts'+$rootScope.globals.currentUser.accountNo+'transfer', {amount: transfer_coin, accountTo:transfer_id,description: transfer_description,signature:transfer_signature }).then(handleSuccessTran, handleErrTran);
            $('.modal').modal('hide');
        };


        function handleSuccessTran(res) {
            $scope.dataLoading = false;
            $scope.queryTransfer();
        }

        function handleErrTran(res) {
            alert('Error');
        }

        $scope.queryTransfer =function () {
            console.log($rootScope.globals);
            $http.get(config.apiUrl+'/accounts/'+'7b79b8cde46035bc7db2ae4250748f3a'+'/history').then(handleSuccessQueryTran, handleErrQueryTran);
        };

        function handleSuccessQueryTran(response) {
            $rootScope.transfers = response.data;
            console.log($rootScope.transfers);
            $scope.dataLoading = false;
        }

        function handleErrQueryTran(response) {
            $rootScope.transfers = [];
        }



    }]);
